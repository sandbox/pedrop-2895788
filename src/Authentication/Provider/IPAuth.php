<?php

namespace Drupal\ip_auth\Authentication\Provider;

use Drupal\Core\Authentication\AuthenticationProviderInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\Entity\User;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * IP authentication provider.
 */
class IPAuth implements AuthenticationProviderInterface {

  /**
   * A configuration object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs an IP authentication provider object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   */
  public function __construct(ConfigFactoryInterface $config_factory, AccountInterface $current_user, LoggerInterface $logger) {
    $this->config = $config_factory->get('ip_auth.settings');
    $this->currentUser = $current_user;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(Request $request): bool {
    $ip_auth = $this->config->get('ip_auth');
    $ip = $request->getClientIp();

    return \in_array($ip, $ip_auth, FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function authenticate(Request $request) {
    $current_user = $this->currentUser;
    if (!$current_user->isAnonymous()) {
      // @todo check if this is really neeeded
      return $current_user;
    }

    $ip_auth = $this->config->get('ip_auth');
    $uid = $this->config->get('uid');

    $ip = $request->getClientIp();

    if (\in_array($ip, $ip_auth, FALSE)) {
      $account = User::load($uid);
      if (\is_object($account)) {
        user_login_finalize($account);
        return $account;
      }
      else {
        $this->logger->error('User uid: @uid does not exist.', ['@uid' => $uid]);
        throw new AccessDeniedHttpException();
      }
    }
    else {
      throw new AccessDeniedHttpException();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function handleException(GetResponseForExceptionEvent $event) {
    $exception = $event->getException();
    if ($exception instanceof AccessDeniedHttpException) {
      $event->setException(new UnauthorizedHttpException('Invalid IP.', $exception));
      return TRUE;
    }
    return FALSE;
  }

}
