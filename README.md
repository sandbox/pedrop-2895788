INTRODUCTION
------------

Drupal 8 custom Authentication provides "seamless" login for a user using IP.


REQUIREMENTS
------------

There is no other module dependency.


INSTALLATION
------------

Use git clone, or composer require:
```
composer require drupal/ip_auth:dev-8.x-1.x
```


CONFIGURATION
-------------

You need to put configuration to your settings.php:
```
// IP Auth - the IP address(es) to authenticate.
$config['ip_auth.settings']['ip_auth'] = ['181.182.183.184'];
// IP Auth - the user id which will be logged in automatically by IP address.
$config['ip_auth.settings']['uid'] = 376;
```
